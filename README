README LPJmL

This file contains some basic information on the open source distribution of the computer simulation model LPJmL.
LPJmL is developed and maintained at the Potsdam Institute for Climate Impact Research (PIK) in Potsdam, Germany.

All source code, configuration and parameter files are subject to 
copyright (C) by the Potsdam Institute for Climate Impact Research, see the file COPYRIGHT
and are licensed under the GNU AFFERO GENERAL PUBLIC LICENSE Version 3, see the file LICENSE
and is the result of collaborative work, see the file AUTHORS

For setting up the model, see the file INSTALL

The source code is distributed via a git repository at https://gitlab.pik-potsdam.de/lpjml/lpjml after registration.
The registration is free of costs.

Outside joint collaborative agreements with PIK, there is absolutely no support in model download, setup, development, application or similar. 

New model development can be submitted to the LPJmL git repository in separate repository branches and pull requests can be issued. There is no guarantee or obligation that external model features, bug fixes or other developemnts will be merged into the standard LPJmL distribution at PIK.
Discussions on model development features can only be initiated via issues at https://gitlab.pik-potsdam.de/lpjml/lpjml after registration.

We strongly encourrage considering the coding standards and style recommendations in LPJmL. For details see the file STYLESHEET

We are interested in learning how much and where LPJmL is used. 
For that we have included a call-home function in configure.[sh|bat] and in the Makefile. 
For technical reasons, we only track the execution of configure.[sh|bat] and model builds using the makefiles, not model runs. 
No personal data is recorded that could be used to identify users, 
only the country in which the scripts/Makefiles are executed and the repository source 
(PIK gitlab open source repository, PIK's internal gitlab repository, PIK's internal SVN repository).
You can find the usage statistics online at https://goo.gl/#analytics/goo.gl/DYv3KW/all_time. 
If you don't want to contribute to the usage statistics, please run configure.sh with the option -nofeedback
or disable the option directly in src/Makefile.


